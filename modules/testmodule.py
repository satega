class TestModule(object):
	def __init__(self):
		self.someVariable = 'Hello'
		
	def about(self):
		return "This module is a testmodule used during initial communication framework development."
		
	def someMethod(self):
		return self.someVariable
	
	def setSomeVariable(self, data):
		self.someVariable = data
