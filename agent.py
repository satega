#
# Copyright (C) 2008
# Torbjorn Soiland <tosoil@start.no>
# Knut Saua Mathiesen <ks.mathiesen@gmail.com>
#
# This file is part of Satega. 
#
# Satega is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Satega is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Satega.  If not, see <http://www.gnu.org/licenses/>.
#


import SimpleXMLRPCServer
import UserDict

class ModulesList(UserDict.UserDict):
	""" A dictionary which holds every module loaded. """
	def __init__(self):
		UserDict.UserDict.__init__(self)
		self.moduledir = __import__('modules')
		self.data = {}
		
	def load(self, filename, name):
		self.data[name] = getattr(__import__('modules.%s' % filename, fromlist=[self.moduledir]), name)()
	

def rpc(modulename, function, *args):
	""" This is the only method that can be called from the frontend. It routes calls to the modules."""
	# Get modulelist from globals, and get module from list.
	module = globals()['ModulesList'][modulename]
	# Get function from the module
	function = getattr(module, function)
	
	# TODO - block access to modules if not logged in
	# TODO - maby do some format checking on data.

	# Call the function. Send return values directly to frontend.
	return function(*args)

if __name__ == '__main__':
	# We get this list later with globals()
	ModulesList = ModulesList()
	ModulesList.load('testmodule', 'TestModule')
	
	server = SimpleXMLRPCServer.SimpleXMLRPCServer(("localhost", 8000), allow_none=True)
	server.register_function(rpc)
	print 'Listening on port 8000'	
	server.serve_forever()
