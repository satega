#
# Copyright (C) 2008
# Torbjorn Soiland <tosoil@start.no>
# Knut Saua Mathiesen <ks.mathiesen@gmail.com>
#
# This file is part of Satega. 
#
# Satega is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Satega is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Satega.  If not, see <http://www.gnu.org/licenses/>.
#

import xmlrpclib

server = xmlrpclib.ServerProxy("http://localhost:8000")
print server.rpc("TestModule", "about")
print server.rpc("TestModule", "someMethod")
print 'Setting some variabel'
server.rpc("TestModule", "setSomeVariable", "Bonjour")
print server.rpc("TestModule", "someMethod")

